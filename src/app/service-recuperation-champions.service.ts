import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import Champion from './class/Champion';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceRecuperationChampionsService {

  constructor(private http: HttpClient) {}

  getChampions(): Observable<Champion[]> {
    return this.http.get<Champion[]>(`${environment.lolapi}/champions`);
  }

  getChampionById(id: number): Observable<Champion> {
    return this.http.get<Champion>(`${environment.lolapi}/champions/id/${id}`);
  }

  getChampionsByRole(role: string): Observable<Champion[]> {
    return this.http.get<Champion[]>(`${environment.lolapi}/champions/role/${role}`);
  }

  getChampionByName(name: string): Observable<Champion> {
    return this.http.get<Champion>(`${environment.lolapi}/champions/name/${name}`);
  }

  searchChampionsByName(search: string): Observable<Champion[]> {
    return this.http.get<Champion[]>(`${environment.lolapi}/champions/search?q=${search}`);
  }

}
