import { Component, Inject } from '@angular/core';
import { ServiceRecuperationChampionsService } from './service-recuperation-champions.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    ServiceRecuperationChampionsService
  ]
})
export class AppComponent {
  constructor() {  }
}
