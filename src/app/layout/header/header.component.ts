import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public title = 'League of Champions';
  public errors = {
    APP_MAY_BE_SLOW_ON_START: 'App may be slow on start cause the host (Heroku) put the app in idle mode when nobody send request to him.',
  };

  constructor() { }

  ngOnInit() {
  }

}
