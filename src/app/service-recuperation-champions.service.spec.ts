import { TestBed, inject } from '@angular/core/testing';

import { ServiceRecuperationChampionsService } from './service-recuperation-champions.service';

describe('ServiceRecuperationChampionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServiceRecuperationChampionsService]
    });
  });

  it('should be created', inject([ServiceRecuperationChampionsService], (service: ServiceRecuperationChampionsService) => {
    expect(service).toBeTruthy();
  }));
});
