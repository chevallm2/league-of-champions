import { Component, OnInit, Input } from '@angular/core';
import { ChampionGridComponent } from '../champion-grid/champion-grid.component';
import { ServiceRecuperationChampionsService } from '../service-recuperation-champions.service';
import Champion from '../class/Champion';

@Component({
  selector: 'app-champion-filter',
  templateUrl: './champion-filter.component.html',
  styleUrls: ['./champion-filter.component.css'],
  providers: [ ChampionGridComponent ]
})
export class ChampionFilterComponent implements OnInit {

  @Input() championGrid: ChampionGridComponent;

  constructor(private serviceChampion: ServiceRecuperationChampionsService) { }

  ngOnInit() { }

  setChampions(champions: Champion[]): void {
    this.championGrid.champions = champions;
  }

  onRoleChange(role: string): void {
    if (role !== 'all') {
      this.serviceChampion.getChampionsByRole(role).subscribe( champions => {
        this.championGrid.champions = champions.map( champion => new Champion().deserialize(champion));
      });
    } else {
      this.serviceChampion.getChampions().subscribe( champions => {
        this.championGrid.champions = champions.map( champion => new Champion().deserialize(champion));
      });
    }
  }

  onSearch(event: any): void {
    const query = event.target.value;
    this.serviceChampion.searchChampionsByName(query).subscribe( champions => {
      this.championGrid.champions = champions.map( champion => new Champion().deserialize(champion));
    });
  }

}
