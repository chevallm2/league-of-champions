import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionFilterComponent } from './champion-filter.component';

describe('ChampionFilterComponent', () => {
  let component: ChampionFilterComponent;
  let fixture: ComponentFixture<ChampionFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChampionFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
