import { Component, OnInit, Input } from '@angular/core';
import Champion from '../class/Champion';
import { ActivatedRoute } from '@angular/router';
import { ServiceRecuperationChampionsService } from '../service-recuperation-champions.service';

@Component({
  selector: 'app-champion-detail',
  templateUrl: './champion-detail.component.html',
  styleUrls: ['./champion-detail.component.css']
})
export class ChampionDetailComponent implements OnInit {

  @Input() champion: Champion;

  constructor(private route: ActivatedRoute, private serviceChampion: ServiceRecuperationChampionsService) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.serviceChampion.getChampionById(id).subscribe( champion => {
      this.champion = new Champion().deserialize(champion);
    });
  }

  getBlurb(): string {
    return this.champion.blurb.replace('<br><br>', '\n');
  }

}
