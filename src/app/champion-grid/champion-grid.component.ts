import { Component, OnInit } from '@angular/core';
import Champion from '../class/Champion';
import { ServiceRecuperationChampionsService } from '../service-recuperation-champions.service';

@Component({
  selector: 'app-champion-grid',
  templateUrl: './champion-grid.component.html',
  styleUrls: ['./champion-grid.component.css']
})
export class ChampionGridComponent implements OnInit {

  public champions: Champion[];

  constructor(private serviceChampions: ServiceRecuperationChampionsService) { }

  ngOnInit() {
    this.serviceChampions.getChampions().subscribe(champions => {
      this.champions = champions.map( champion => new Champion().deserialize(champion));
    });
  }

}
