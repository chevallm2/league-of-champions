import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionGridComponent } from './champion-grid.component';

describe('ChampionGridComponent', () => {
  let component: ChampionGridComponent;
  let fixture: ComponentFixture<ChampionGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChampionGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
