import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ServiceRecuperationChampionsService } from './service-recuperation-champions.service';
import { HeaderComponent } from './layout/header/header.component';
import { ChampionFilterComponent } from './champion-filter/champion-filter.component';
import { ChampionGridComponent } from './champion-grid/champion-grid.component';
import { ChampionThumbnailComponent } from './champion-thumbnail/champion-thumbnail.component';
import { ChampionDetailComponent } from './champion-detail/champion-detail.component';
import { ChampionHomeComponent } from './champion-home/champion-home.component';
import { RouterModule, Routes } from '@angular/router';
import { AlertComponent } from './alert/alert.component';
import { FooterComponent } from './layout/footer/footer.component';

const appRoutes: Routes = [
  { path: '', component: ChampionHomeComponent},
  { path: 'champion/:id', component: ChampionDetailComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ChampionFilterComponent,
    ChampionGridComponent,
    ChampionThumbnailComponent,
    ChampionDetailComponent,
    ChampionHomeComponent,
    AlertComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
    )
  ],
  providers: [
    ServiceRecuperationChampionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
