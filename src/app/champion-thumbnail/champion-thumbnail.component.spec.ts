import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionThumbnailComponent } from './champion-thumbnail.component';

describe('ChampionThumbnailComponent', () => {
  let component: ChampionThumbnailComponent;
  let fixture: ComponentFixture<ChampionThumbnailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChampionThumbnailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionThumbnailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
