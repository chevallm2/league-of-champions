import { Component, OnInit, Input } from '@angular/core';
import Champion from '../class/Champion';

@Component({
  selector: 'app-champion-thumbnail',
  templateUrl: './champion-thumbnail.component.html',
  styleUrls: ['./champion-thumbnail.component.css']
})
export class ChampionThumbnailComponent implements OnInit {

  @Input() champion: Champion;

  constructor() { }

  ngOnInit() { }

}
