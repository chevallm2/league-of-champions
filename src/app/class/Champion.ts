import { Deserializable } from './deserializable.model';

export default class Champion implements Deserializable {

  public version: string;
  public id: string;
  public key: string;
  public name: string;
  public title: string;
  public blurb: string;
  public info: {
    attack: number;
    defense: number;
    magic: number;
    difficulty: number;
  };
  public image: {
    full: string;
    sprite: string;
    group: string;
    x: number,
    y: number;
    w: number;
    h: number;
  };
  public tags: string[];
  public partype: string;
  public stats: {
    hp: number;
    hpperlevel: number;
    mp: number;
    mpperlevel: number;
    movespeed: number;
    armor: number;
    armorperlevel: number;
    spelllock: number;
    spellblockperlevel: number;
    attackrange: number;
    hpregen: number;
    hpregenperlevel: number;
    mpregen: number;
    mpregenperlevel: number;
    crit: number;
    critperlevel: number;
    attackdamage: number;
    attackdamageperlevel: number;
    attackspeedoffset: number;
    attackspeedperlevel: number;
  };

  constructor() { }

  deserialize(input: any) {
    Object.assign(<any>this, input);
    return this;
  }
}
